
import cv2
import numpy as np
import tello
import os
import time
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class Vision(QThread):
    changePixmap = QtCore.pyqtSignal(QImage)
    def run(self):
        while True:
            global drone, face_cascade, automatico
            frame = drone.read()
            if frame is not None:
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                cv2.normalize(gray, gray, 0, 255,cv2.NORM_MINMAX)
                faces = face_cascade.detectMultiScale(gray, 1.3, 5)
                global objetos
                objetos = faces

                for (x,y,w,h) in faces:
                    cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2)
                    cv2.circle(frame,(x,y),5,(0,0,255),-1)

                rgbImage = frame
                convertToQtFormat = QImage(rgbImage.data, rgbImage.shape[1], rgbImage.shape[0], QImage.Format_RGB888)
                p = convertToQtFormat.scaled(640, 480, Qt.KeepAspectRatio)
                self.changePixmap.emit(p)
