#!/usr/bin/env python2
import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from interfaz import Ui_VentanaPrincipal
import cv2
import numpy as np
import tello
import os
import time
from matplotlib.backends.backend_qt5agg import (FigureCanvas, NavigationToolbar2QT as NavigationToolbar) #Matplotlib
from matplotlib.figure import Figure
import matplotlib

drone = tello.Tello('', 8889)
face_cascade = cv2.CascadeClassifier(os.path.dirname(os.path.realpath(__file__)) + '/haarcascade_frontalface_alt.xml')
automatico = False
incrementodistancia = 0.20
incrementogrados = 10
objetos = []
tamanoconocido = 4.3
distanciafocal = 923
from simple_pid import PID
matplotlib.use('TkAgg')
class Controlador(QThread):
    senalcontrol = QtCore.pyqtSignal(float)
    def __init__(self, Kp=1.0, Kd=0.1, Ki = 0.05, sp=1, sampletime=0.33):
        QThread.__init__(self)
        self.pid = PID(Kp=Kp, Kd=Kd, Ki=Ki, setpoint=sp)
        self.calcular = False
        self.distancia = 0
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd
        self.SP = sp
        self.paramPID = (self.Kp, self.Ki, self.Kd)
        self.finalizar = False
    def run(self):
        print "Inicio Thread"
        while not self.finalizar:
            if self.calcular:
                control = -self.pid(self.distancia)
                self.senalcontrol.emit(control)
                self.calcular = False
                print "Variable de control " + str(control)
                print "Distancia rostro: " + str(self.distancia)
                print "Param Ki" + str(self.pid.Ki)
                print "Param Kd" + str(self.pid.Kd)
                print "Param Kp" + str(self.pid.Kp)
                print "SetPoint: " + str(self.pid.setpoint)
            time.sleep(0.03)
        print "Finalizo Self Thread"
        self.finalizar = False
    def __del__(self):
        self.wait()
    def setearDistancia(self, distancia):
        self.distancia = distancia
        self.calcular = True
    def setearParametrosPID(self, paramPID):
        self.paramPID = paramPID
        self.pid.tunings = self.paramPID
    def setearKi(self, Ki):
        self.Ki = Ki
        self.pid.Ki = self.Ki
    def setearKd(self, Kd):
        self.Kd = Kd
        self.pid.Kd = self.Kd
    def setearKp(self, Kp):
        self.Kp = Kp
        self.pid.Kp = self.Kp
    def setearSP(self, SP):
        self.SP = SP
        self.pid.setpoint = self.SP


class Vision(QThread):
    changePixmap = QtCore.pyqtSignal(QImage)
    rostrotamano = QtCore.pyqtSignal(int)

    def __init__(self):
        QThread.__init__(self)
    def run(self):
        while True:
            global drone, face_cascade, automatico
            frame = drone.read()
            if frame is not None:
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                cv2.normalize(gray, gray, 0, 255,cv2.NORM_MINMAX)
                faces = face_cascade.detectMultiScale(gray, 1.3, 5)
                global objetos
                objetos = faces
                tamanorostro = -1
                for (x,y,w,h) in faces:
                    cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2)
                    cv2.circle(frame,(x,y),5,(0,0,255),-1)
                    print "ancho: " + str(w)
                    print "alto: " + str(h)
                    tamanorostro = (tamanoconocido*distanciafocal)/w

                self.rostrotamano.emit(tamanorostro)
                rgbImage = frame
                convertToQtFormat = QImage(rgbImage.data, rgbImage.shape[1], rgbImage.shape[0], QImage.Format_RGB888)
                p = convertToQtFormat.scaled(640, 480, Qt.KeepAspectRatio)
                self.changePixmap.emit(p)
    def __del__(self):
        self.wait()


class VentanaPrincipal(QtWidgets.QMainWindow):
    def __init__(self):
        super(VentanaPrincipal, self).__init__()
        # Set up the user interface from Designer.
        self.ui = Ui_VentanaPrincipal()
        self.ui.setupUi(self)
        dynamic_canvas = FigureCanvas(Figure(figsize=(5, 3)))
        self.ui.grafico.addWidget(dynamic_canvas)
        #self.addToolBar(QtCore.Qt.BottomToolBarArea,
        #                NavigationToolbar(dynamic_canvas, self))

        self._dynamic_ax = dynamic_canvas.figure.subplots()
        self._dynamic_ax.clear()
        self.ui.botonAterrizarmano.clicked.connect(self.aterrizarmano)
        self.ui.botonAterrizar.clicked.connect(self.aterrizar)
        self.ui.botonDespegar.clicked.connect(self.despegar)
        self.ui.botonManual.clicked.connect(self.manual)
        self.ui.botonAutomatico.clicked.connect(self.automatico)
        self.ui.botonAdelante.clicked.connect(self.adelante)
        self.ui.botonAtras.clicked.connect(self.atras)
        self.ui.giroIzquierda.clicked.connect(self.girarizquierda)
        self.ui.giroDerecha.clicked.connect(self.girarderecha)
        self.ui.botonArriba.clicked.connect(self.arriba)
        self.ui.botonAbajo.clicked.connect(self.abajo)
        self.ui.botonIzquierda.clicked.connect(self.izquierda)
        self.ui.botonDerecha.clicked.connect(self.derecha)
        self.ui.spinDistancia.valueChanged.connect(self.cambioDistancia)
        self.ui.spinGrados.valueChanged.connect(self.cambioGrados)
        self.ui.iniciarControl.clicked.connect(self.iniciarControl)
    	#conectamos el boton de enviar datos con la funcion que mueve lineal y angular
        self.ui.pushButton.clicked.connect(self.moverlineal)
        self.ui.botonManual.setDisabled(True)
        self.ui.botonAutomatico.setDisabled(True)
        self.ui.botonAterrizarmano.setDisabled(True)
        self.ui.botonAterrizar.setDisabled(False)
        self.ui.botonDespegar.setDisabled(False)
        self.ui.botonAutomatico.setDisabled(False)
        self.ui.botonAdelante.setDisabled(True)
        self.ui.botonAtras.setDisabled(True)
        self.ui.giroIzquierda.setDisabled(True)
        self.ui.giroDerecha.setDisabled(True)
        self.ui.botonArriba.setDisabled(True)
        self.ui.botonAbajo.setDisabled(True)
        self.ui.botonIzquierda.setDisabled(True)
        self.ui.botonDerecha.setDisabled(True)
        self.timer = QTimer()
        self.timer.timeout.connect(self.actualizarbateria)
        self.timer.start(500)
        self.timer1 = QTimer()
        self.timer1.timeout.connect(self.actualizarpunto)
        self.timer1.start(30)
        self.threadVision = Vision()
        self.threadVision.changePixmap.connect(lambda p: self.ui.label.setPixmap(QPixmap(p)))
        self.threadVision.rostrotamano.connect(self.rostrotamano)
        self.threadVision.start()
        self.vectorsetpoint = []
        self.vectordistancias = []
        self.vectortiempos = []
        self.anteriortamano = 0
        #self._timer = dynamic_canvas.new_timer(100, [(self._update_canvas, (), {})])
        #self._timer.start()
        self.threadPID = Controlador()
        self.threadPID.senalcontrol.connect(self.enviarControl)

    def _update_canvas(self):
        self._dynamic_ax.clear()
        self._dynamic_ax.plot(self.vectortiempos, self.vectordistancias, 'b', label='Distancia')
        self._dynamic_ax.plot(self.vectortiempos, self.vectorsetpoint, 'r--', label='SetPoint')
        self._dynamic_ax.figure.canvas.draw()

    def aterrizarmano(self):
        print("Sin implementar")
    def aterrizar(self):
        drone.land()
        self.ui.botonAterrizar.setDisabled(False)
        self.ui.botonDespegar.setDisabled(False)
        self.ui.botonManual.setDisabled(True)
        self.ui.botonAutomatico.setDisabled(True)
        self.ui.botonAdelante.setDisabled(True)
        self.ui.botonAtras.setDisabled(True)
        self.ui.giroIzquierda.setDisabled(True)
        self.ui.giroDerecha.setDisabled(True)
        self.ui.botonArriba.setDisabled(True)
        self.ui.botonAbajo.setDisabled(True)
        self.ui.botonIzquierda.setDisabled(True)
        self.ui.botonDerecha.setDisabled(True)
        self.actualizarbateria()
    def despegar(self):
        drone.takeoff()
        self.ui.botonAterrizar.setDisabled(False)
        self.ui.botonDespegar.setDisabled(True)
        self.ui.botonManual.setDisabled(False)
        self.ui.botonAutomatico.setDisabled(False)
        self.ui.botonAdelante.setDisabled(False)
        self.ui.botonAtras.setDisabled(False)
        self.ui.giroIzquierda.setDisabled(False)
        self.ui.giroDerecha.setDisabled(False)
        self.ui.botonArriba.setDisabled(False)
        self.ui.botonAbajo.setDisabled(False)
        self.ui.botonIzquierda.setDisabled(False)
        self.ui.botonDerecha.setDisabled(False)
        self.actualizarbateria()
    def manual(self):
        global automatico
        automatico = False
        self.ui.botonManual.setDisabled(True)
        self.ui.botonAutomatico.setDisabled(False)
        self.ui.botonAterrizarmano.setDisabled(False)
        self.ui.botonAterrizar.setDisabled(False)
        self.ui.botonDespegar.setDisabled(False)
        self.ui.botonAdelante.setDisabled(False)
        self.ui.botonAtras.setDisabled(False)
        self.ui.giroIzquierda.setDisabled(False)
        self.ui.giroDerecha.setDisabled(False)
        self.ui.botonArriba.setDisabled(False)
        self.ui.botonAbajo.setDisabled(False)
        self.ui.botonIzquierda.setDisabled(False)
        self.ui.botonDerecha.setDisabled(False)
        self.actualizarbateria()
    def automatico(self):
        global automatico
        automatico = True
        self.ui.botonManual.setDisabled(False)
        self.ui.botonAutomatico.setDisabled(True)
        self.ui.botonAterrizarmano.setDisabled(True)
        self.ui.botonAterrizar.setDisabled(False)
        self.ui.botonDespegar.setDisabled(True)
        self.ui.botonAdelante.setDisabled(True)
        self.ui.botonAtras.setDisabled(True)
        self.ui.giroIzquierda.setDisabled(True)
        self.ui.giroDerecha.setDisabled(True)
        self.ui.botonArriba.setDisabled(True)
        self.ui.botonAbajo.setDisabled(True)
        self.ui.botonIzquierda.setDisabled(True)
        self.ui.botonDerecha.setDisabled(True)
        self.actualizarbateria()
    def adelante(self):
        drone.move("forward", self.ui.spinDistancia.value())
        self.actualizarbateria()
    def atras(self):
        drone.move("back", self.ui.spinDistancia.value())
        self.actualizarbateria()
    def girarizquierda(self):
        drone.rotate_ccw(self.ui.spinGrados.value())
        self.actualizarbateria()
    def girarderecha(self):
        drone.rotate_cw(self.ui.spinGrados.value())
        self.actualizarbateria()
    def izquierda(self):
        drone.move("left", self.ui.spinDistancia.value())
        self.actualizarbateria()
    def derecha(self):
        drone.move("right", self.ui.spinDistancia.value())
        self.actualizarbateria()
    def arriba(self):
        drone.move_up(self.ui.spinDistancia.value())
        self.actualizarbateria()
    def abajo(self):
        drone.move_down(self.ui.spinDistancia.value())
        self.actualizarbateria()
    def cambioDistancia(self):
        incrementodistancia = self.ui.spinDistancia.value()
        self.actualizarbateria()
    def cambioGrados(self):
        incrementogrados = self.ui.spinGrados.value()
    def actualizarbateria(self):
        valor = drone.get_battery()
        if isinstance(valor, int): #Solo si es entero
            self.ui.barraBateria.setValue(valor)

    def moverlineal(self):
        if self.dir_x.value() != 0 or self.dir_y.value() != 0 or  self.dir_x.value() != 0:
            drone.send_command("go "+ str(self.dir_x.value()) + " " + str(self.dir_y.value()) + " " + str(self.dir_z.value()) + " 100")
        if self.dir_angular.value() != 0:
            if self.dir_angular.value() > 0:
                drone.rotate_cw(self.dir_angular.value())
            else:
                drone.rotate_ccw(-self.dir_angular.value())
            time.sleep(2)
        self.dir_x.setValue(0)
        self.dir_y.setValue(0)
        self.dir_z.setValue(0)
        self.dir_angular.setValue(0)
    def actualizarpunto(self):
        global objetos
        self.ui.valor_pixel.setText("")
        for(x,y,w,h) in objetos:
            self.ui.valor_pixel.setText("x: " + str(x) + " y: " + str(y) + " w:" + str(w) + " h: " + str(h))
    def rostrotamano(self, tamano):
        'Almacena la distancia del rostro al drone'
        if tamano < 0:
            tamano = self.anteriortamano
            drone.rotate_cw(15)
        else:
            self.vectortiempos.append(time.time())
            self.vectordistancias.append(tamano)
            self.vectorsetpoint.append(self.ui.sp.value())
            self.anteriortamano = tamano
            self.ui.distanciaRostro.setText(str(tamano))

            if self.threadPID.isRunning():
                self.threadPID.setearSP(self.ui.sp.value())
                self.threadPID.setearDistancia(tamano)

            if len(self.vectortiempos) > 100:
                self.vectortiempos.pop(0)
                self.vectordistancias.pop(0)
                self.vectorsetpoint.pop(0)
    def iniciarControl(self):
        print self.ui.iniciarControl.text()
        print self.ui.iniciarControl.text().find("Iniciar")
        if self.ui.iniciarControl.text().find("Iniciar") > 0:
            self.threadPID.setearParametrosPID((self.ui.kp.value(), self.ui.ki.value(), self.ui.kd.value()))
            self.threadPID.finalizar = False
            if not self.threadPID.isRunning():
                self.threadPID.start()
            self.ui.iniciarControl.setText("Fin Control")
        else:
            self.ui.iniciarControl.setText("Iniciar Control")
            self.threadPID.finalizar = True
    def enviarControl(self, control):
        self.ui.salidaPID.setText(str(control))
        if control > 0.0:
            drone.move_forward((control/100))
            print "Mover adelante: " + str(control) + "cm"
        elif control < 0.0:
            drone.move_backward(-(control/100))
            print "Mover atras: " + str(-control) + "cm"
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    Ventana = QtWidgets.QMainWindow()
    ui = VentanaPrincipal()
    ui.show()
    sys.exit(app.exec_())
